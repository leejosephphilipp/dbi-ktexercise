package hk.com.novare.repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;

import hk.com.novare.pojo.Test;

/**
 * Class implementing {@link TestRepository} methods
 */
@Repository
public class TestRepositoryImpl implements TestRepository {
	@Autowired
	DynamoDBMapper dbMapper;

	@Override
	public void save(Test test) {
		dbMapper.save(test);
	}

	@Override
	public void delete(Test test) {
		dbMapper.delete(test);
	}

	@Override
	public Test find(String id) {
		return dbMapper.load(Test.class, id);
	}

	@Override
	public List<Test> findAll() {
		return dbMapper.scan(Test.class, new DynamoDBScanExpression());
	}

	@Override
	public List<Test> findByData(String data) {
		Map<String, String> ean = new HashMap<>();
		Map<String, AttributeValue> eav = new HashMap<>();

		ean.put("#d", "data");
        eav.put(":data", new AttributeValue().withS(data));

		return dbMapper.scan(Test.class, new DynamoDBScanExpression().withFilterExpression("#d = :data")
																	 .withExpressionAttributeNames(ean)
																	 .withExpressionAttributeValues(eav));
	}

	@Override
	public List<Test> findByDate(Date date) {
		Map<String, String> ean = new HashMap<>();
		Map<String, AttributeValue> eav = new HashMap<>();

		ean.put("#d", "date");
        eav.put(":date", new AttributeValue().withS(formatDate(date)));

		return dbMapper.scan(Test.class, new DynamoDBScanExpression().withFilterExpression("#d = :date")
																	 .withExpressionAttributeNames(ean)
																	 .withExpressionAttributeValues(eav));
	}

	@Override
	public List<Test> findById(String id) {
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
        eav.put(":id", new AttributeValue().withS(id));

		return dbMapper.scan(Test.class, new DynamoDBScanExpression().withFilterExpression("Id = :id")
																	 .withExpressionAttributeValues(eav));
	}

	@Override
	public List<Test> findByIdDataDate(String id, String data, Date date) {
		Map<String, String> ean = new HashMap<>();
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		
		ean.put("#d", "data");
		ean.put("#da", "date");
		
        eav.put(":id", new AttributeValue().withS(id));
        eav.put(":data", new AttributeValue().withS(data));
        eav.put(":date", new AttributeValue().withS(formatDate(date)));

		return dbMapper.scan(Test.class, new DynamoDBScanExpression()
												.withFilterExpression("Id = :id AND #d = :data AND #da = :date")
												.withExpressionAttributeNames(ean)
												.withExpressionAttributeValues(eav));
	}
	
	private String formatDate(Date date) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormatter.format(date);
	}
}
