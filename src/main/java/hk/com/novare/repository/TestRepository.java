package hk.com.novare.repository;

import java.util.Date;
import java.util.List;

import hk.com.novare.pojo.Test;

/**
 * Interface defining data operations for {@link Test} items
 */
public interface TestRepository {
	/**
	 * Saves a {@link Test} item in DynamoDB
	 * @param item The {@link Test} item to save.
	 */
	void save(Test item);
	
	/**
	 * Deletes a {@link Test} item from DynamoDB
	 * @param item The {@link Test} item to delete.
	 */
	void delete(Test item);
	
	/**
	 * Retrieves a {@link Test} item in DynamoDB
	 * @param id The ID of item to retrieve.
	 * @return The {@link Test} item with the given ID.
	 */
	Test find(String id);
	
	/**
	 * Retrieves all {@link Test} items in DynamoDB
	 * @return A {@code List} of all {@link Test} items in DynamoDB
	 */
	List<Test> findAll();
	
	/**
	 * Retrieves all {@link Test} items in DynamoDB by their data
	 * @param data The term to search the Data column by.
	 * @return  A {@code List} of all {@link Test} items with the given data
	 */
	List<Test> findByData(String data);
	
	/**
	 * Retrieves all {@link Test} items in DynamoDB by their date
	 * @param date The term to search the Date column by.
	 * @return  A {@code List} of all {@link Test} items with the given date
	 */
	List<Test> findByDate(Date date);
	
	/**
	 * Retrieves all {@link Test} item in DynamoDB
	 * @param id The ID of item to retrieve.
	 * @return  A {@code List} of all {@link Test} items with the given ID.
	 */
	List<Test> findById(String id);
	
	/**
	 * Retrieves all {@link Test} items in DynamoDB by their id, data and date
	 * @param id The ID of item to retrieve.
	 * @param data The term to search the Data column by.
	 * @param date The term to search the Date column by.
	 * @return  A {@code List} of all {@link Test} items with the id, data and date
	 */
	List<Test> findByIdDataDate(String id, String data, Date date);
}
