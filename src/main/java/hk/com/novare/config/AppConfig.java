package hk.com.novare.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

/**
 * Configuration class for setting up the DynamoDB and S3 beans
 */
@Configuration
public class AppConfig {
	/**
	 * The AWS region to use as configured in application.properties
	 */
	@Value("${aws.region}")
	private String awsRegion;
	
	/**
	 * The AWS access key to use as configured in application.properties
	 */
	@Value("${aws.access-key-id}")
	private String awsAccessKey;
	
	/**
	 * The AWS secret key to use as configured in application.properties
	 */
	@Value("${aws.secret-access-key}")
	private String awsSecretKey;
	
	/**
	 * Initializes the {@link DynamoDBMapper} bean using the 
	 * AWS configuration specified in application.properties 
	 * @return
	 */
	@Bean
	public DynamoDBMapper configureDynamoDbClient() {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
				.withRegion(awsRegion)
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
	            .build();
		
		return new DynamoDBMapper(client);
	}
	
	
	/**
	 * Initializes the {@link AmazonS3} bean using the 
	 * AWS configuration specified in application.properties
	 * @return
	 */
	@Bean
	public AmazonS3 configureS3Client() {
		AmazonS3 client = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
                .withRegion(awsRegion)
                .build();
		
		return client;
	}
}
