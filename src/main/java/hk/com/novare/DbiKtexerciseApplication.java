package hk.com.novare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Spring Boot application class used to bootstrap the application
 */
@SpringBootApplication
public class DbiKtexerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbiKtexerciseApplication.class, args);
	}
}
