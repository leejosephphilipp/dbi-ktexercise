package hk.com.novare.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import hk.com.novare.pojo.Test;
import hk.com.novare.service.CipherService;
import hk.com.novare.service.S3Service;
import hk.com.novare.service.TestService;

/**
 * REST Controller class defining request handlers for 
 * resources under /novare_test
 */
@RequestMapping("novare_test")
@RestController
public class TestController {
	@Autowired
	private TestService testService;
	
	@Autowired
	private S3Service s3Service;
	
	@Autowired
	private CipherService cipherService;
	
	/**
	 * Resource listing all {@link Test} items
	 * @return A JSON array of all {@link Test} items
	 */
	@GetMapping("/read")
	public List<Test> readAll() {
		return testService.findAll();
	}
	
	/**
	 * Resource retrieving a {@link Test} item by ID
	 * @param id The ID of the item to retrieve
	 * @return A JSON object of the {@link Test} item with the given ID
	 */
	@GetMapping("/read/{id}")
	public Test read(@PathVariable String id) {
		return testService.find(id);
	}
	
	/**
	 * Resource retrieving a {@link Test} item by ID
	 * @param id The ID of the item to retrieve
	 * @return A JSON object of the {@link Test} item with the given ID
	 */
	@GetMapping("/read/id")
	public List<Test> findById(@RequestParam String id) {
		return testService.findById(id);
	}
	
	/**
	 * Resource retrieving all {@link Test} item by data
	 * @param data The term to search the Data column by
	 * @return A JSON array of all {@link Test} items with the given data
	 */
	@GetMapping("/read/data")
	public List<Test> findByData(@RequestParam String data) {
		return testService.findByData(data);
	}
	
	/**
	 * Resource retrieving all {@link Test} item by date
	 * @param date The term to search the Date column by, in ISO format
	 * @return A JSON array of all {@link Test} items with the given date
	 */
	@GetMapping("/read/date")
	public List<Test> findByDate(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date date,
								 @RequestParam(required = false) boolean saveToS3) throws Exception {
		return testService.findByDate(date, saveToS3);
	}
	
	/**
	 * Resource retrieving all {@link Test} item by id, data and date
	 * @param id The ID of the item to retrieve
	 * @param data The term to search the Data column by
	 * @param date The term to search the Date column by, in ISO format
	 * @return A JSON array of all {@link Test} items with the given date
	 */
	@GetMapping("/read/find")
	public List<Test> findByDate(@RequestParam String id,
								 @RequestParam String data,
								 @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date date) {
		return testService.findByIdDataDate(id, data, date);
	}
	
	/**
	 * Resource creating a new {@link Test} item
	 * @param test The {@link Test} item to create
	 */
	@PostMapping("/create")
	public void create(@RequestBody Test test) {
		testService.save(test);
	}
	
	/**
	 * Resource updating an existing {@link Test} item
	 * @param test The {@link Test} item to update
	 */
	@PostMapping("/update")
	public void update(@RequestBody Test test) {
		testService.save(test);
	}
	
	/**
	 * Resource deleting a new {@link Test} item
	 * @param test The {@link Test} item to delete
	 */
	@PostMapping("/delete")
	public void delete(@RequestBody Test test) {
		testService.delete(test);
	}
	
	/**
	 * Resource uploading a multipart file to S3
	 * @param file The file to upload
	 */
	@PostMapping("/upload")
	public void upload(@RequestParam MultipartFile file) throws IOException {
		s3Service.upload(file);
	}
	
	/**
	 * Resource encrypting the given file using AES encryption
	 * @param file The file to encrypt
	 */
	@PostMapping("/encrypt")
	public ResponseEntity<String> encrypt(@RequestParam MultipartFile file) throws IOException {
		return ResponseEntity.ok()
							.header(HttpHeaders.CONTENT_DISPOSITION,
									"attachment; filename=\"" + System.currentTimeMillis() +
										"-" + file.getOriginalFilename() + "\"")
							.body(cipherService.encrypt(new String(file.getBytes())));
	}
	
	/**
	 * Resource decrypting the given file using AES encryption
	 * @param file The file to decrypt
	 */
	@PostMapping("/decrypt")
	public ResponseEntity<String> decrypt(@RequestParam MultipartFile file) throws IOException {
		return ResponseEntity.ok()
							.header(HttpHeaders.CONTENT_DISPOSITION,
									"attachment; filename=\"" + System.currentTimeMillis() + 
										"-" + file.getOriginalFilename() + "\"")
							.body(cipherService.decrypt(new String(file.getBytes())));
	}
}