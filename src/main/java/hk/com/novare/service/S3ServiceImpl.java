package hk.com.novare.service;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;

/**
 * Class implementing {@link S3Service} methods
 */
@Service
public class S3ServiceImpl implements S3Service {
	@Autowired
	private AmazonS3 s3Client;
	
	@Value("${aws.s3.bucketName}")
	private String bucketName;
	
	@Override
	public void upload(MultipartFile file) throws IOException {
		try (InputStream is = file.getInputStream()){
			s3Client.putObject(bucketName, file.getOriginalFilename(), is, new ObjectMetadata());
		} catch (IOException e) {
			throw e;
		}
	}

	@Override
	public void upload(String json) {
		s3Client.putObject(bucketName, System.currentTimeMillis() + ".txt", json);
	}
}
