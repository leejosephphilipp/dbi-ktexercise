package hk.com.novare.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Class implementing {@link CipherService} methods
 */
@Service
public class CipherServiceImpl implements CipherService {	
	@Value("${cipher.key}")
	private String key;
	
	private SecretKeySpec aesKey;
	
	private Cipher aesCipher;

	@PostConstruct
	public void setup() throws Exception {
		aesKey = new SecretKeySpec(Base64.getDecoder().decode(key), "AES");
		aesCipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	}
	
	@Override
	public String encrypt(String data) {
		try {
			byte[] iv = new byte[16];
			new SecureRandom().nextBytes(iv);
			
			aesCipher.init(Cipher.ENCRYPT_MODE, aesKey, new IvParameterSpec(iv));

			byte[] encrypted = aesCipher.doFinal(data.getBytes("UTF-8"));
			
			Encoder encoder = Base64.getEncoder();
			encrypted = encoder.encode(encrypted);
			iv = encoder.encode(iv);
			
			return new String(iv) + "|" + new String(encrypted);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException | 
				IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
			return null;
		}
	}

	@Override
	public String decrypt(String data) {
		try {
			Decoder decoder = Base64.getDecoder();

			byte[] iv = decoder.decode(data.substring(0, data.indexOf('|')));
			data = data.substring(data.indexOf('|') + 1, data.length());
			aesCipher.init(Cipher.DECRYPT_MODE, aesKey, new IvParameterSpec(iv));
	
			byte[] decrypted = aesCipher.doFinal(decoder.decode(data));
			
			return new String(decrypted);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException | 
				IllegalBlockSizeException | BadPaddingException e) {
			return null;
		}
	}
}
