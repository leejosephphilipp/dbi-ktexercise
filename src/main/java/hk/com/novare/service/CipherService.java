package hk.com.novare.service;

/**
 * Interface defining AES cipher operations.
 */
public interface CipherService {
	/**
	 * Encrypts the given data using the configured key with AES encryption
	 * @param data The data to encrypt
	 * @return The encrypted data
	 */
	String encrypt(String data);
	
	/**
	 * Decrypts the given data using the configured key with AES encryption
	 * @param data The data to decrypt
	 * @return The decrypted data
	 */
	String decrypt(String data);
}