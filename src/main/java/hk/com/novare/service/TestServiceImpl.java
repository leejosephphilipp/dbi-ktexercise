package hk.com.novare.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import hk.com.novare.pojo.Test;
import hk.com.novare.repository.TestRepository;

/**
 * Class implementing {@link TestService} methods
 */
@Service
public class TestServiceImpl implements TestService {
	@Autowired
	private TestRepository testDao;
	
	@Autowired
	private S3Service s3Service;
	
	@Autowired
	private CipherService cipherService;
	
	@Autowired
	private ObjectMapper objMapper;
	
	@Override
	public void save(Test item) {
		testDao.save(item);
	}

	@Override
	public void delete(Test item) {
		testDao.delete(item);
	}

	@Override
	public Test find(String id) {
		return testDao.find(id);
	}

	@Override
	public List<Test> findAll() {
		List<Test> result = testDao.findAll();
		return result;
	}

	@Override
	public List<Test> findByData(String data) {
		List<Test> result = testDao.findByData(data);
		return result;
	}

	@Override
	public List<Test> findByDate(Date date, boolean saveToS3) {
		List<Test> result = testDao.findByDate(date);
		if (saveToS3) {
			try {
				s3Service.upload(cipherService.encrypt(objMapper.writeValueAsString(result)));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<Test> findById(String id) {
		List<Test> result = testDao.findById(id);
		return result;
	}

	@Override
	public List<Test> findByIdDataDate(String id, String data, Date date) {
		List<Test> result = testDao.findByIdDataDate(id, data, date);
		return result;
	}
}
