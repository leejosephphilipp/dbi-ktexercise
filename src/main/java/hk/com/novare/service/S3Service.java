package hk.com.novare.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

/**
 * Interface defining S3 operations
 */
public interface S3Service {
	/**
	 * Uploads a {@link MultipartFile} to S3 in the configured bucket.
	 * @param file The file to upload
	 * @throws IOException If file cannot be temporarily stored
	 */
	void upload(MultipartFile file) throws IOException;
	
	/**
	 * Uploads a JSON string to S3 in the configured bucket.
	 * @param json The JSON string to upload
	 */
	void upload(String json);
}
